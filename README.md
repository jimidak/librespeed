# LibreSpeed

### Use their online Speedtest site to test, from your endpoint, to various LibreSpeed sites  
https://librespeed.org/  

### Codebase to LibreSpeed  
https://github.com/librespeed/speedtest  

### How it compares to closed source self hosted speed test applications  
__ookla__ is a leading software for selfhosted speedtests. It is a costly licensed product  
compairing _librespeed_ to _ookla_, libraspeed presents slightly higher Jitter numbers on average  
The way libraspeed calculates Jitter is more accurate than ookla, although a test should be run several times on Libre to get an accurate idea of Jitter.  

From ookla:  
```
This test is performed by measuring the time it takes for the server to reply to a request from the user's client. The client sends a message to the server, upon receiving that message, the server sends a reply back. The round-trip time is measured in ms (milliseconds).
This test is repeated multiple times with the lowest value determining the final result.
```

From LibreSpeed:  
```
Jitter is calculated from one out-and-back test.  
 
```
&nbsp; _becuase of this, you should probablly do several consecutive LibreSpeed tests to get a feel for Jitter._ 

Note: _You may also compare LibreSpeed with speedtest applications you may get from your internet service providor_  


### other cloud speedtest sites  
&nbsp; &nbsp; cloudflare  
&nbsp; &nbsp; xfinity  

### other self hosts FOSS speedtest software 
&nbsp; &nbsp; openspeedtest  
&nbsp; &nbsp; Speedtest-Tracker

### Jitter 
Network jitter is inconsistant or unpredictable latency. This can be measured from a continious ping from a source to destination.  
If every ping return had a `time=` of around 15 milliseconds, you would say there is no Jitter. If ping times varied (ex. 5ms for one ping, 37ms for the next ping, etc.), you would say there is Jitter between those points.  

Jitter won't affect most network funtions, but for streaming functions, like UCaaS, Video, Audio, Conferencing, etc., Jitter can result in poor quality or buffering.  

If you find you have Jitter, you may work with the hosting provider on bandwidth improvements. If you are self hosted, you can tune networks, or host in a different location.  If you are geo-redundant, and you notice Jitter to one location, you can switch to the other location.  

It's hard to give a "good jitter" number, since its a function of laterncy, but looks for under 45ms on average.  

### Latency 
A good network latency number is enerally under 200ms. I document on how to manually calculate some of these things.  
https://www.pingplotter.com/wisdom/article/is-my-connection-good  


<br>  
<br>  

## LibreSpeed installation  

(These are server install instructions. I am instlling on VMs (KVM).  Some choose phys server installs for more acurate results.  There is also a LibreSpeed container on Docker Hub)  

Overview. 
You can install on any Linux distro  
Install Apache2 and PHP is it's not already on Linux.  
git Clone librespeed  
copy 3 things to the document root of Apache; backed directory, which example index file you want, and all .js files  
change ownership of document root recursivly  


- `cd /srv`  

- `git clone https://github.com/librespeed/speedtest.git`  

- `cd speedtest`  

- `cp -R backend example-singleServer-pretty.html *.js /var/www/html`  

- `cd /var/www/html`  

- `mv example-singleServer-pretty.html index.html`  

- `chown -R www-data *`  

- (restart apache2 if necessary)


### additional considerations  

you may care to put speedtest servers listening on non standard ports, with you can configure in Apache.  
you may care to put a TLS certificate on your speedtest/Apache server, and have a URL only on HTTPS  
you may care to have an OS firewall to filter for only speedtest (443 or 80 or non std). 
you might consider installing `fail2ban` since this server may get accessed publically, frequently    


### example index options

There are several options of index files you can copy to /var/www/html  
single, denotes a single speedtest instance, while multiple is for utilizing more than one.  
within multiple, there is one that will autoselect from multiple, and one that provides a dropdown for you to select.  (both select the closest one by default)  
pretty denotes ones with progress bars  
full gives more indexes than the base 4 (upload, download, latency, jitter)  


### telemetry  
if you want to log historical speed numbers, librespeed has the ability to store results to a database  

### speedtest results  
librespeed by default, for most example indexes, provides __laterncy__, __jitter__, __upload test__ and __download test__, as well as information on the source ISP.  


